import pygame


class Image(pygame.sprite.Sprite):
    def __init__(self, image_file=None, location=None, dimensions=None):
        pygame.sprite.Sprite.__init__(self)  #call Sprite initializer
        if image_file != None:
            self.image = pygame.image.load(image_file)
            self.rect = self.image.get_rect()
            self.rect.left, self.rect.top = location
        else:
            self.image = pygame.Surface(dimensions, pygame.SRCALPHA)
            self.rect = self.image.get_rect()
    def move(self,location):
        self.rect.left, self.rect.top = location
    def center(self, parent):
        p_width, p_height = parent.get_size()
        return ( (p_width-self.rect.width)/2, (p_height-self.rect.height)/2   ) 
