import time

class FormattedTime:
    def formatTime(time_in_ms):
        return time.strftime('%M:%S:{}'.format(time_in_ms%1000), time.gmtime(time_in_ms/1000.0))