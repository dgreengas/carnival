#!/usr/bin/env python3

import pygame
import pygame_textinput
import os, fnmatch
from image import Image
from utilities.constants import GameMode, JoystickButton
from utilities.timedisplay import FormattedTime
from player import Player
import uuid
import random

import shadow

def main():
    #balloon_height_fudge = 30
    #balloon_width_fudge = 5
    #shadow_ambiance = .65
    face_from_stand = 35
    #shadow_offset = (30, 30)
    clock_pos = (40, 20)
    players = []
    game_id = None
    winning_message=None
    pygame.mixer.pre_init() #(frequency=44100,size=16,channels=2,buffer=4096)
    pygame.init()

    
    pygame.joystick.init()
    pygame.font.init()
    
    font_instructions = pygame.font.SysFont('Linux Libertine', 30)
    font_countdown = pygame.font.SysFont('Noto Sans', 125)
    font_timer = pygame.font.SysFont('Noto Sans', 60)
    countdown_start_ticks = 0
    joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
    #initialize the joysticks
    for joystick in joysticks:
        joystick.init()

    logo = pygame.image.load("logo.png")
    pygame.display.set_icon(logo)
    pygame.display.set_caption("Welcome to the Carnival!")

    screen = pygame.display.set_mode((1920,1080),0,32) #, pygame.FULLSCREEN | pygame.HWSURFACE)
    #screen = pygame.display.set_mode((200,200),0,32)
    running = True
    bg_color = (249,249,249)
    color_black = (20,20,20)
    color_white = (255,255,255)
    clock = pygame.time.Clock()
    #textinput = pygame_textinput.TextInput()
    backdrop = Image( os.path.join('sprites', 'backdrop.png' ), [0,0])
    playerOneStand = Image( os.path.join('sprites', 'stand.png'), [1920*3/8, 1080*5/8])
    playerTwoStand = Image( os.path.join('sprites', 'stand.png'), [1920*5/8, 1080*5/8])
    #playerOneBalloon = Image( os.path.join('sprites', 'blue_balloon_100.png'), [0,0] )
    #playerTwoBalloon = Image( os.path.join('sprites', 'red_balloon_100.png'), [0,0] )
    face_1 = None
    face_2 = None
    #face_1 = Image(os.path.join('sprites', 'face_1.png'), [0,0])
    #face_2 = Image(os.path.join('sprites', 'face_1.png'), [0,0])
    #move Balloon based on stand
    #playerOneBalloon.move( [ playerOneStand.rect.left + (playerOneStand.rect.width - playerOneBalloon.rect.width)/2 - balloon_width_fudge,  playerOneStand.rect.top - playerOneBalloon.rect.height + balloon_height_fudge ]  )
    #playerTwoBalloon.move( [ playerTwoStand.rect.left + (playerTwoStand.rect.width - playerTwoBalloon.rect.width)/2 - balloon_width_fudge,  playerTwoStand.rect.top - playerTwoBalloon.rect.height + balloon_height_fudge ]  )
    #move face_1 left of stand one
    #face_1.move( [ playerOneStand.rect.left - face_from_stand - face_1.rect.width, playerOneStand.rect.top + playerOneStand.rect.height - face_1.rect.height    ])
    #move face_2 right of stand two
    #face_2.move( [ playerTwoStand.rect.left + playerTwoStand.rect.width + face_from_stand, playerTwoStand.rect.top + playerTwoStand.rect.height - face_2.rect.height    ])
    explosion = Image(os.path.join('sprites', 'explosion.png'), [0,0])
    explosion.move(( (screen.rect.width-explosion.rect.width)/2, (screen.rect.height-explosion.rect.height)/2 ) )

    #inflateSoundFile = os.path.join('sounds', 'inflated-balloon-short.wav')
    #inflateSound = pygame.mixer.Sound(inflateSoundFile)

    #initialize players
    players[0] = Player("Player 1","blue",screen, playerOneStand, joysticks[0] if len(joysticks) > 0 else None)
    players[1] = Player("Player 2", "red", screen, playerTwoStand, joysticks[1] if len(joysticks) > 1 else None)

    game_mode = GameMode.MODE_IDLE
    countdown_value = 3
    game_start_ms=0
    while running:
        events = pygame.event.get()
        #pygame.display.update
        if game_mode == GameMode.MODE_IDLE:
            screen.fill(color_black)
            for event in events:
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYUP or event.type == pygame.locals.JOYBUTTONUP:
                    game_mode=GameMode.MODE_COUNTDOWN
                    countdown_start_ticks = pygame.time.get_ticks()

            instructions = font_instructions.render("Press any key to start playing. Use alternate joystick buttons to inflate your balloon.", False, color_white)
            screen.blit(instructions, ( (screen.get_width()-instructions.get_width())/2, (screen.get_height()-instructions.get_height()  )/2 ) )

        elif game_mode == GameMode.MODE_COUNTDOWN:
            screen.fill(color_black)
            
            countdown = font_countdown.render(str(countdown_value),False, color_white)
            countdown.center(screen)
            screen.blit(countdown, countdown.center(screen))
            current_ticks = pygame.time.get_ticks()
            if current_ticks - countdown_start_ticks >= 1000:
                countdown_value -= 1
                countdown_start_ticks = current_ticks

            if countdown_value == 0:
                game_mode = GameMode.MODE_PLAYING
                game_id = uuid.uuid4()
                players[0].startGame(game_id)
                players[1].startGame(game_id)
                #random faces
                face_1 = Image(getRandomFace(), [0,0])
                face_1.move( [ playerOneStand.rect.left - face_from_stand - face_1.rect.width, playerOneStand.rect.top + playerOneStand.rect.height - face_1.rect.height    ])
                face_2 = Image(getRandomFace(), [0,0])
                face_2.move( [ playerTwoStand.rect.left + playerTwoStand.rect.width + face_from_stand, playerTwoStand.rect.top + playerTwoStand.rect.height - face_2.rect.height    ])




                game_start_ms=pygame.time.get_ticks()

        elif game_mode == GameMode.MODE_PLAYING:
            screen.fill(bg_color)
            screen.blit(backdrop.image, backdrop.rect)
            #blit shadows in the back
            #shadowBalloonOne = shadow.make_shadow(playerOneBalloon.image, shadow_ambiance)
            #screen.blit(shadow.place_shadow(playerOneBalloon.image, shadowBalloonOne, shadow_offset), playerOneBalloon.rect)
            
            #shadowBalloonTwo = shadow.make_shadow(playerTwoBalloon.image, shadow_ambiance)
            #screen.blit(shadow.place_shadow(playerTwoBalloon.image, shadowBalloonTwo, shadow_offset), playerTwoBalloon.rect)
            #screen.blit(playerOneBalloon.image, playerOneBalloon.rect)
            #screen.blit(playerTwoBalloon.image, playerTwoBalloon.rect)
            players[0].processEvent(events)
            players[1].processEvent(events)

            screen.blit(playerOneStand.image, playerOneStand.rect)
            screen.blit(playerTwoStand.image, playerTwoStand.rect)
            


            screen.blit(face_1.image, face_1.rect)
            screen.blit(face_2.image, face_2.rect)
            #screen.fill((225, 0, 0))
            elapsed_time=pygame.time.get_ticks()-game_start_ms
            game_time = font_timer.render( FormattedTime.formatTime( elapsed_time), False, color_white )

            screen.blit(game_time, clock_pos)

            for event in events:
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYUP or event.type == pygame.locals.JOYBUTTONUP:
                    if event.key == pygame.K_i:
                        #inflate sound
                        pass
                        #print("playing sound ...")
                        #inflateSound.play()
                    elif event.key == pygame.K_f:
                        #toggle full screen
                        pygame.display.toggle_fullscreen()
                    elif event.key == pygame.K_q:
                        #return to home when user presses q in a game
                        game_mode = GameMode.MODE_IDLE

                    #joybutton test
                    for joystick in joysticks:
                        if joystick.get_button(JoystickButton.BUTTON_START):
                            game_mode = GameMode.MODE_IDLE

            playerOnePopped = players[0].balloon.isPopped()
            playerTwoPopped = players[1].balloon.isPopped()
            if playerOnePopped or playerTwoPopped :
                game_mode = GameMode.MODE_WINNER
                if playerOnePopped and not playerTwoPopped:
                    winning_message = "Player One Wins"
                elif playerTwoPopped and not playerOnePopped:
                    winning_message = "Player Two Wins"
                else:
                    winning_message = "Tie Game!"

                if playerOnePopped:
                    players[0].setWinner(elapsed_time)
                
                if playerTwoPopped:
                    players[0].setWinner(elapsed_time)
                

        elif game_mode == GameMode.MODE_WINNER:
            screen.fill(bg_color)
            screen.blit(backdrop.image, backdrop.rect)
            screen.blit(explosion.image, explosion.rect)
            winning_surface = font_countdown.render(winning_message, False, color_black)
            screen.blit(winning_surface, ((screen.rect.width-winning_surface.rect.width)/2, (screen.rect.height-winning_surface.rect.height)/2))
            for event in events:
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYUP or event.type == pygame.locals.JOYBUTTONUP:
                    game_mode = GameMode.IDLE
        pygame.display.flip()                    
        clock.tick(20)
def getRandomFace():
    randomNumber = random.randint(1,100)
    if randomNumber >= 80:
        listOfFiles = os.listdir(os.path.join('sprites'))
        pattern = "face_*.png"
        matches = []
        for entry in listOfFiles:
            if fnmatch.fnmatch(entry,pattern):
                matches.append(entry)
        randomFace = random.randint(0, len(matches)-1)
        return os.path.join('sprites', matches[randomFace])
    else:
        return os.path.join('sprites', 'face_1.png')


if __name__ == "__main__":
    main()