import pygame
import os
from image import Image
from utilities.constants import ShadowConstants
import shadow


class Balloon():
    def __init__(self, screen, balloon_color, stand, inflateInterval=None):
        self._isPopped = False
        self.inflatedPercent = 0
        if inflateInterval is None:
            self.inflateInterval = 10
        else:
            self.inflateInterval = inflateInterval
        
        #Sprite.__init__(self)
        self.screen = screen

        self.balloon_color = balloon_color
        self.images = {}
        self.images_w = {}
        self.images_h = {}
        self.stand = stand
        #self.update_rect()
        self.balloon_height_fudge = 30
        self.balloon_width_fudge = 5
        self.target_width = 512
        self.target_height = 512

        self.__loadSounds()
        self.__loadImages()
        self.drawZero = True


    def inflate(self):
        self.inflatedPercent += self.inflateInterval
        #sounds played in checkPopped
        #self.inflateSound.stop()
        #self.inflateSound.play()
        self.checkPopped()


    def inflateTo(self, inflateto):
        self.inflatedPercent = inflateto
        self.checkPopped()

    def checkPopped(self):
        if self.inflatedPercent >= 100:
            self._isPopped = True
            self.inflateSound.stop()
            print("burst")
            self.burstSound.play()
            if self.inflatedPercent == 1000:
                #self.burstSound.play()
                while pygame.mixer.get_busy():
                    pass
                self.secretSound.play()
        else:
            if pygame.mixer.get_busy() == False:
                self.inflateSound.play()
        #self.update_rect()

    def isPopped(self):
        return self._isPopped

    def deflate(self):
        self.inflatedPercent = 0
        self._isPopped = False

    def getInflatedPercent(self):
        return self.inflatedPercent

    def blitme(self):
        if self._isPopped:
            #draw explosion
            #self.screen.blit( self.explosion.image, self.explosion.rect )
            #self.burstSound.play()
            pass
        elif self.inflatedPercent <= 0:
            #draw only images 0
            if self.drawZero:
                print("draw 0")
                self.drawZero = False
            shadowBalloon = shadow.make_shadow(self.images[0].image, ShadowConstants.SHADOW_AMBIANCE)
            self.screen.blit(shadow.place_shadow(self.images[0].image, shadowBalloon, ShadowConstants.SHADOW_OFFSET), self.images[0].rect)
            self.screen.blit(self.images[0].image, self.images[0].rect)
        else:
            #draw base followed by images 0
            #shadowBase = shadow.make_shadow(self.image_base.image, ShadowConstants.SHADOW_AMBIANCE)
            shadowBalloon = shadow.make_shadow(self.images[self.inflatedPercent].image, ShadowConstants.SHADOW_AMBIANCE)
            #self.screen.blit(shadow.place_shadow(self.image_base.image, shadowBase, ShadowConstants.SHADOW_OFFSET), self.image_base.rect)
            self.screen.blit(shadow.place_shadow(self.images[self.inflatedPercent].image, shadowBalloon, ShadowConstants.SHADOW_OFFSET), self.images[self.inflatedPercent].rect)

    #def update_rect(self):
    #    self.rect = pygame.Rect(self.x_position - self.images_w[self.inflatedPercent]/2, 
    #                            self.y_position - self.images_h[self.inflatedPercent]/2,
    #                            self.images_w[self.inflatedPercent],
    #                            self.images_h[self.inflatedPercent])
    def __loadSounds(self):
        inflateSoundFile = os.path.join('sounds', 'inflated-balloon-short.wav')
        burstSoundFile = os.path.join('sounds', 'balloon-burst.wav')
        self.inflateSound = pygame.mixer.Sound(inflateSoundFile)
        self.burstSound = pygame.mixer.Sound(burstSoundFile)
        self.secretSound = pygame.mixer.Sound(os.path.join('sounds','finish-him.wav'))

    def __loadImages(self):
        index = 0
        self.images[0] = Image(os.path.join('sprites', 'balloon_' + self.balloon_color + '_0' + '.png'), (0,0))
        self.images[0].move(( [ self.stand.rect.left + (self.stand.rect.width - self.images[0].rect.width)/2,
                                        self.stand.rect.top - self.images[0].rect.height + self.balloon_height_fudge*3]) ) 
        #self.explosion = Image(os.path.join('sprites', 'explosion.png'), (0,0))
        #self.explosion.move ( (self.stand.rect.left + (self.stand.rect.width - self.explosion.rect.width)/2 , self.stand.rect.top - self.target_height ) )
        #self.image_base = Image(os.path.join('sprites', 'balloon_' + self.balloon_color + '_base.png'), (0,0))
        #self.image_base.move( [ self.stand.rect.left + (self.stand.rect.width - self.image_base.rect.width)/2 - self.balloon_width_fudge,
        #                                self.stand.rect.top - self.image_base.rect.height + self.balloon_height_fudge])
        print("loading image " + os.path.join('sprites', 'balloon_' + self.balloon_color + '_1.png'))
        image_start = Image(os.path.join('sprites', 'balloon_' + self.balloon_color + '_1.png'), (0,0))
        image_start_width = image_start.rect.width
        image_start_height = image_start.rect.height
        width_ratio = self.target_width/ image_start_width / 100
        height_ratio = self.target_height/ image_start_height / 100
        while index <= 100:
            if index > 0:
                new_rect = (  int(image_start_width * ( 1 + index *  width_ratio  )), int(image_start_height * ( 1+ index*height_ratio)) )
                self.images[index] = Image(image_file=None, location=None, dimensions=new_rect)
                
                pygame.transform.smoothscale(image_start.image, new_rect, self.images[index].image  )
                self.images_w[index], self.images_h[index] = self.images[index].image.get_size()
                self.images[index].move(( [ self.stand.rect.left + (self.stand.rect.width - self.images[index].rect.width)/2 - self.balloon_width_fudge,
                                        self.stand.rect.top - self.images[index].rect.height + self.balloon_height_fudge]) )
            index += self.inflateInterval