import pygame
from balloon import Balloon
from utilities.constants import JoystickButton
import math
import requests
import json

class Player:
    def __init__(self, playerName, playerPosition, screen, stand, joystick = None):
        self.playerName = playerName
        self.playerPosition = playerPosition
        print(self.playerName, self.playerPosition)
        self.balloon = Balloon(screen, playerPosition, stand, 1)
        #self.textInput = textInput
        self.joystick = joystick
        self.buttons = []
        self.button_up = True
    def getBalloon(self):
        return self.balloon

    def getPosition(self):
        return self.playerPosition

    def setWinner(self, elapsedTime):
        result_payload={'gameid': str(self.gameid), 'winner': self.playerName, 'duration': str(elapsedTime)}
        req=requests.post(url="https://us-central1-nice-stack-226004.cloudfunctions.net/carnival_balloon_score_update", data=json.dumps(result_payload), headers={'Content-Type':'application/json'})
        if req.text == 'Error: could not handle the request\n':
            req=requests.post(url="https://us-central1-nice-stack-226004.cloudfunctions.net/carnival_balloon_score_update", data=json.dumps(result_payload), headers={'Content-Type':'application/json'})
        print(req.text)


    def startGame(self, gameid):
        self.gameid = gameid
        self.balloon.deflate()
        self.buttons = []
    
    def processEvent(self,events):
        for event in events:
            if event.type == pygame.locals.JOYBUTTONDOWN:
                # check to see if it is our gamepad
                for button in range(0, self.joystick.get_numbuttons()-1):
                    if self.joystick.get_button(button) == True:
                        
                        self.buttons.append(button)
                        if self.shouldInflate():
                            self.balloon.inflate()
                self.button_up = False
            elif event.type == pygame.locals.JOYBUTTONUP:
                self.button_up = True
            elif event.type == pygame.locals.JOYAXISMOTION:
                #check for navigation
                #use ceiling because up and right show up as 0.999969482421875
                x,y = math.ceil(self.joystick.get_axis(0)), math.ceil(self.joystick.get_axis(1))
                if x == 1 and y == 0:
                    self.buttons.append(JoystickButton.BUTTON_NAV_RIGHT)
                elif x == 0 and y == -1:
                    self.buttons.append(JoystickButton.BUTTON_NAV_UP)
                elif x == -1 and y == 0:
                    self.buttons.append(JoystickButton.BUTTON_NAV_LEFT)
                elif x == 0 and y == 1:
                    self.buttons.append(JoystickButton.BUTTON_NAV_DOWN)

        if self.isSecretSequence():
            #instant win due to secret code
            self.balloon.inflateTo(1000)

        self.balloon.blitme()
    
    def isSecretSequence(self):
        #return true if secret sequence of buttons in self.buttons
        #up,up,down,down,left,right,left,right,b,a
        if len(self.buttons) < 10:
            return False
        elif len(self.buttons) > 10:
            self.buttons = self.buttons[1:]
        #elif len(self.buttons) == 10:
        #    print(self.buttons)
        #remaining button count should be ten
        if (self.buttons[0] == JoystickButton.BUTTON_NAV_UP and
           self.buttons[1] == JoystickButton.BUTTON_NAV_UP and
           self.buttons[2] == JoystickButton.BUTTON_NAV_DOWN and
           self.buttons[3] == JoystickButton.BUTTON_NAV_DOWN and
           self.buttons[4] == JoystickButton.BUTTON_NAV_LEFT and
           self.buttons[5] == JoystickButton.BUTTON_NAV_RIGHT and
           self.buttons[6] == JoystickButton.BUTTON_NAV_LEFT and
           self.buttons[7] == JoystickButton.BUTTON_NAV_RIGHT and
           self.buttons[8] == JoystickButton.BUTTON_B and
           self.buttons[9] == JoystickButton.BUTTON_A):
           return True
        else:
            return False

    def shouldInflate(self):
        if len(self.buttons) < 2:
            #must click 2 different buttons
            return False
        else:
            #check that the 0 and 1 are different
            if self.buttons[-1] != self.buttons[-2] and self.button_up:
                return True
            else:
                return False